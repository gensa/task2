package com.example.abdullah.task2;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap map;
    final float DEFAULT_ZOOM_LEVEL = 13;
    final float MARKER_ZOOM_THRESHOLD = 15;
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    BitmapDescriptor marker;
    Geocoder geoCodeInfo;
    Marker dropPoint;

    public void zoomIn(View v) {
        if(map != null)
            map.moveCamera(CameraUpdateFactory.zoomIn());
    }
    public void zoomOut(View v) {
        if(map!=null)
            map.moveCamera(CameraUpdateFactory.zoomOut());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.hybrid :
                if(map != null)
                    map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.map_action :
                if(map != null)
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_maps);
        marker = BitmapDescriptorFactory.fromResource(R.mipmap.marker);
        dropPoint = null;
        geoCodeInfo = new Geocoder(this, Locale.getDefault());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("الخريطة");
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST) {
            if ((permissions.length == 1 &&
                    permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED ) ||
                    (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                map.setMyLocationEnabled(true);
            } else {
            }
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                if (map.getCameraPosition().zoom >= MARKER_ZOOM_THRESHOLD) {
                    if (dropPoint != null)
                        dropPoint.remove();
                    try {
                        insertMarker(latLng, getPlaceInfo(geoCodeInfo.getFromLocation(latLng.latitude, latLng.longitude, 1)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else
                    Toast.makeText(getApplicationContext(), R.string.marker_zoom_message, Toast.LENGTH_LONG).show();
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(permission, LOCATION_PERMISSION_REQUEST);
        } else if(map!=null) {
            map.setMyLocationEnabled(true);
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            String locationProvider = LocationManager.NETWORK_PROVIDER;
            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()),DEFAULT_ZOOM_LEVEL));
        }
    }

    private String getPlaceInfo(List<Address> fromLocation) {
        String output ="";
        output += fromLocation.get(0).getAddressLine(0) + ", ";
        output += fromLocation.get(0).getLocality() + ", ";
        output += fromLocation.get(0).getPostalCode() + " ";
        return output;
    }


    private void insertMarker(LatLng destination, String destinationTitle) {
        MarkerOptions location = new MarkerOptions().position(destination).title(destinationTitle).icon(marker);
        dropPoint = map.addMarker(location);
    }

}
